$(function(){
    $('.getting-started-form-switcher').on('click', function(){
        var $context = $(this),
            formId = $(this).data('form');

        $('.getting-started-form-switcher').removeClass('active');
        $context.addClass('active');

        $('.getting-started-form').removeClass('active');
        $('#' + formId).addClass('active');

        return false;
    })
});