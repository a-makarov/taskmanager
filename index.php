<?php
require_once(__DIR__ . "/application/bootstrap.php");

global $application, $layout;

$layout->init();
$application->route();
$layout->close();
