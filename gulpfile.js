'use strict';

const gulp = require('gulp');
const sourcemaps = require('gulp-sourcemaps');
const sass = require('gulp-sass');
const cleancss = require('gulp-clean-css');
const autoprefixer = require('gulp-autoprefixer');
const rigger = require("gulp-rigger");
const uglify = require("gulp-uglify");
const notify = require("gulp-notify");
const gulpIf = require("gulp-if");
const del = require("del");
const multipipe = require("multipipe");

const isDevelopment = !process.env.NODE_ENV || process.env.NODE_ENV == 'development';

var path = {};
path.base = {};

path.base.backend = 'application/';
path.base.backendAssets = path.base.backend + '**/assets/**/';
path.base.build = 'public/';
path.base.dev = 'frontend/';
path.base.bower = 'bower_components/';
path.base.bootstrap = path.base.bower + 'bootstrap-sass/assets/';

path.build = {
    js: path.base.build + 'js/',
    styles: path.base.build + 'styles/',
    images: path.base.build + 'images/',
    fonts: path.base.build + 'fonts/'
};
path.dev ={
    js: [
        path.base.dev + 'js/main.js'
    ],
    styles: [
        path.base.dev + 'styles/main.scss'
    ],
    images: [
        path.base.dev + 'images/**/*.*',
        path.base.bootstrap + 'images/**/*.*'
    ],
    fonts: [
        path.base.dev + 'fonts/**/*.*',
        path.base.bootstrap + 'fonts/**/*.*'
    ]
};
path.watch = {
    js: [
        path.base.dev + 'js/**/*.js'
    ],
    styles: [
        path.base.dev + 'styles/**/*.scss'
    ],
    images: [
        path.base.dev + 'images/**/*.*',
        path.base.bootstrap + 'images/**/*.*'
    ],
    fonts: [
        path.base.dev + 'fonts/**/*.*',
        path.base.bootstrap + 'fonts/**/*.*'
    ]
};

gulp.task('styles', function(){
    return multipipe(
        gulp.src(path.dev.styles),
        gulpIf(isDevelopment, sourcemaps.init()),
        sass(),
        autoprefixer(),
        gulpIf(!isDevelopment, cleancss()),
        gulpIf(isDevelopment, sourcemaps.write()),
        gulp.dest(path.build.styles)
    ).on('error', notify.onError());
});

gulp.task('js', function(){
    return multipipe(
        gulp.src(path.dev.js),
        rigger(),
        gulpIf(isDevelopment, sourcemaps.init()),
        gulpIf(!isDevelopment, uglify()),
        gulpIf(isDevelopment, sourcemaps.write()),
        gulp.dest(path.build.js)
    ).on('error', notify.onError());
});

gulp.task('fonts', function() {
    return multipipe(
        gulp.src(path.dev.fonts),
        gulp.dest(path.build.fonts)
    ).on('error', notify.onError());
});

gulp.task('images', function() {
    return multipipe(
        gulp.src(path.dev.images),
        gulp.dest(path.build.images)
    ).on('error', notify.onError());
});


gulp.task('clean', function() {
    return del(path.base.build);
});

gulp.task('build', gulp.series(
    'clean',
    gulp.parallel('styles', 'js', 'fonts', 'images')
));

gulp.task('watch', function(){
    gulp.watch(path.watch.js, gulp.series('js'));
    gulp.watch(path.watch.styles, gulp.series('styles'));
    gulp.watch(path.watch.images, gulp.series('images'));
    gulp.watch(path.watch.fonts, gulp.series('fonts'));
});

gulp.task('dev', gulp.series('build', 'watch'));