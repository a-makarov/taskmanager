<?php

require_once (__DIR__ . "/application/doctrine_init.php");

global $EM;
return \Doctrine\ORM\Tools\Console\ConsoleRunner::createHelperSet($EM);
