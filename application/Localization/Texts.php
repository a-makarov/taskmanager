<?php

namespace Localization;


class Texts
{
    const ENTRY = "Вход";
    const REGISTRATION = "Регистрация";
    const TO_SUBMIT = "Отправить";
    const TO_LOGIN = "Войти";
    const TO_REGISTER = "Зарегистрироваться";
    const EMAIL = "Эл. почта";
    const PASSWORD = "Пароль";
    const CONFIRM_PASSWORD = "Пароль еще раз";
    const PASSWORDS_NOT_EQUAL = "Пароли не совпадают";
    const INVALID_LOGIN_OR_PASSWORD = "Неверный логин или пароль";
    const PASSWORD_MUST_BE_FORMATTED = "Пароль должен быть не менее 6 символов в длину";
    const INVALID_EMAIL = "Неверный адрес эл. почты";
    const USER_WITH_EMAIL_ALREADY_EXIST = "Пользователь с таким email уже существует";
    const YOUR_NAME = "Ваше имя";
    const PROFILE = "Личный кабинет";
}