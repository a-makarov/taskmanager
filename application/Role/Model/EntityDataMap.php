<?php

namespace Role\Model;

use Common\DataMap\Entity;
use Project\Model\EntityDataMap as ProjectEntity;

/**
 * @Entity
 * @Table(name="roles")
 *
 * @method int id()
 * @method string|$this name(string $value = null)
 * @method string|$this description(string $value = null)
 * @method ProjectEntity|$this project(ProjectEntity $value = null)
 * @method array users()
 **/
class EntityDataMap extends Entity
{
    /**
     * @var int
     * @Id
     * @Column(type="integer")
     * @GeneratedValue
     */
    protected $id;

    /**
     * @var string
     * @Column(type="string")
     */
    protected $name;

    /**
     * @var string
     * @Column(type="text")
     */
    protected $description;

    /**
     * @var ProjectEntity
     * @ManyToOne(targetEntity="Project\Model\EntityDataMap")
     */
    protected $project;

    /**
     * @var array
     * @ManyToMany(targetEntity="User\Model\EntityDataMap")
     * @JoinTable(
     *     name="user_project_roles",
     *     joinColumns={
     *         @JoinColumn(name="role_id", referencedColumnName="id")
     *     },
     *     inverseJoinColumns={
     *         @JoinColumn(name="user_id", referencedColumnName="id")
     *     }
     * )
     */
    protected $users;
}
