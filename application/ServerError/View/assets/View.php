<?php
use Common\Tools;
use ServerError\View\View;
if(!Tools::isViewAssetInRightContext(get_called_class(), View::class)) return;
/**
 * @var View $this
 */
global $layout;
$layout->setAdditionalClassForMain("diagonal-strips-bg");
?>

<div class="server-error-box">
    <div class="server-error-code">404</div>
    <div class="server-error-message">Страница не найдена</div>
</div>