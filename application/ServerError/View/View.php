<?php

namespace ServerError\View;

use Base\View as BaseView;

class View extends BaseView
{
    public function render()
    {
        include __DIR__ . "/assets/View.php";
    }
}