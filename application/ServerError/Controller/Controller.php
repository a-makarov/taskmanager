<?php

namespace ServerError\Controller;

use Base\Controller as BaseController;
use ServerError\View\View;

class Controller extends BaseController
{
    public function declareViewInstance()
    {
        return new View();
    }
}