<?php

namespace User\Controller;


use Base\Form\Controller\Controller as BaseFormController;
use Localization\Texts;
use User\Model\ProfileFormFieldMap as FormFieldMap;
use User\View\Profile as View;

/**
 * @method View getView()
 */
class Profile extends BaseFormController
{
    public function declareViewInstance()
    {
        return new View();
    }
    
    public function declareFormFieldMap()
    {
        return new FormFieldMap();
    }
    
    public function init()
    {
        global $layout;
        $layout->setTitle(Texts::PROFILE);
        parent::init();
    }

    public function processPost()
    {
        // TODO: Implement processPost() method.
    }
}