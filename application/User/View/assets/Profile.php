<?php
use Common\Tools;
use User\View\Profile as View;
if(!Tools::isViewAssetInRightContext(get_called_class(), View::class)) return;
/**
 * @var View $this
 */
?>

<div class="container">
    <div class="col-md-6">
        <?parent::render()?>
    </div>
</div>