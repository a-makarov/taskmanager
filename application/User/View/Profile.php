<?php

namespace User\View;


use Base\Form\View\View as BaseFormView;

class Profile extends BaseFormView
{
    public function render()
    {
        include __DIR__ . "/assets/Profile.php";
    }
}