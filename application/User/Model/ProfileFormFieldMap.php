<?php

namespace User\Model;


use Base\Form\Model\FormFieldMap as BaseFormFieldMap;
use Common\FieldView\Email;
use Common\FieldView\Password;
use Common\FieldView\Text;
use Localization\Texts;

class ProfileFormFieldMap extends BaseFormFieldMap
{
    public $email;

    public $name;

    public $password;

    public $confirmPassword;

    public function __construct()
    {
        $this->email = new Email();
        $this->email
            ->setFieldName("email")
            ->setQuestionText(Texts::EMAIL);

        $this->name = new Text();
        $this->name
            ->setFieldName("name")
            ->setQuestionText(Texts::YOUR_NAME);

        $this->password = new Password();
        $this->password
            ->setFieldName("password")
            ->setQuestionText(Texts::PASSWORD);

        $this->confirmPassword = new Password();
        $this->confirmPassword
            ->setFieldName("confirm_password")
            ->setQuestionText(Texts::CONFIRM_PASSWORD);
    }
}
