<?php

namespace User\Model;

use Auth\Model\Cookie;
use Auth\Model\Cryptography;
use User\Model\EntityDataMap as UserEntity;

class Model
{
    /**
     * @return UserEntity|null
     */
    public static function getCurrent ()
    {
        if (!Cookie::clientHasAuthData()) {
            return null;
        }
        $userId = Cookie::getId();
        return self::getById($userId);
    }

    /**
     * The method gives a user by id
     * @param int $id
     * @return UserEntity|null
     */
    public static function getById ($id)
    {
        global $EM;
        return $EM->find(UserEntity::class, $id);
    }

    /**
     * @param string $email
     * @return UserEntity|null
     */
    public static function getByEmail ($email)
    {
        global $EM;
        $queryBuilder = $EM->createQueryBuilder()
            ->select("u")
            ->from(UserEntity::class, "u")
            ->where("u.email = :email")
            ->setParameter("email", $email);
        $result = $queryBuilder->getQuery()->getResult();
        return array_shift($result);
    }

    /**
     * @param string $email
     * @return bool
     */
    public static function isUserWithEmailExist ($email)
    {
        $user = self::getByEmail($email);
        return !is_null($user);
    }

    /**
     * @param string $email
     * @param string $password
     * @return UserEntity|null
     */
    public static function getByAuth ($email, $password)
    {
        global $EM;
        $queryBuilder = $EM->createQueryBuilder()
            ->select("u")
            ->from(UserEntity::class, "u")
            ->where("u.email = :email")
            ->setParameter("email", $email)
            ->setMaxResults(1);
        Cryptography::setAuthenticateFilter($queryBuilder, "u.password_hash", "u.password_salt", $password);
        $result = $queryBuilder->getQuery()->getResult();
        return array_shift($result);
    }

    /**
     * @param string $email
     * @param string $password
     * @return UserEntity
     */
    public static function add ($email, $password) 
    {
        return (new UserEntity)
            ->email($email)
            ->createAuthData($password)
            ->save();
    }
}
