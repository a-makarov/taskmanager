<?php

namespace User\Model;

use Auth\Model\Cryptography;
use Common\DataMap\Entity;
use DateTime;
use Doctrine\Common\Collections\ArrayCollection;
use File\Model\EntityDataMap as FileEntity;
use Localization\Texts;
use Respect\Validation\Validator;

/**
 * @Entity
 * @Table(name="users")
 * @HasLifeCycleCallbacks
 *
 * @method int id()
 * @method string    |$this passwordHash(string $value = null)
 * @method string    |$this passwordSalt(string $value = null)
 * @method string    |$this name(string $value = null)
 * @method DateTime  |$this registerDate(DateTime $value = null)
 * @method FileEntity|$this avatarFile(FileEntity $value = null)
 * @method ArrayCollection   sessions()
 **/
class EntityDataMap extends Entity
{
    /**
     * @var int
     * @Id
     * @Column(type="integer")
     * @GeneratedValue
     */
    protected $id;

    /**
     * @var string
     * @Column(type="string", unique=true)
     */
    protected $email;

    /**
     * @var string
     * @Column(type="string", length=128)
     */
    protected $password_hash;

    /**
     * @var string
     * @Column(type="string", length=32)
     */
    protected $password_salt;

    /**
     * @var string
     * @Column(type="string", nullable=true)
     */
    protected $name;

    /**
     * @var \DateTime
     * @Column(type="datetime")
     */
    protected $register_date;

    /**
     * @var FileEntity
     * @OneToOne(targetEntity="\File\Model\EntityDataMap")
     * @Column(nullable=true)
     */
    protected $avatar_file;

    /**
     * @var string
     * @OneToMany(targetEntity="\Session\Model\EntityDataMap", mappedBy="user")
     */
    protected $sessions;

    /**
     * @PrePersist
     */
    function onPrePersist()
    {
        $this->registerDate(new DateTime());
    }

    /**
     * creates and sets salt and hash
     * @param $password
     * @return $this
     */
    public function createAuthData ($password)
    {
        $salt = Cryptography::generateSalt();
        $hash = Cryptography::hashToken($password, $salt);
        $this
            ->passwordHash($hash)
            ->passwordSalt($salt);
        return $this;
    }

    /**
     * @param string $value
     * @param bool $checkExistence
     * @return $this|string
     */
    public function email ($value = null, $checkExistence = true)
    {
        if (is_null($value)) {
            return $this->email;
        }
        $this->assertValidEmail($value);
        if ($checkExistence) {
            $this->assertUserWithEmailNotExist($value);
        }
        $this->email = $value;
        return $this;
    }

    /**
     * @param $email
     * @return $this
     */
    protected function assertValidEmail($email)
    {
        $validator = Validator::email();
        $validator->setTemplate(Texts::INVALID_EMAIL);
        $validator->assert($email);
        return $this;
    }

    /**
     * @param $email
     * @return $this
     */
    protected function assertUserWithEmailNotExist($email)
    {
        $exist = Model::isUserWithEmailExist($email);
        $validator = Validator::falseVal();
        $validator->setTemplate(Texts::USER_WITH_EMAIL_ALREADY_EXIST);
        $validator->assert($exist);
        return $this;
    }
}
