<?php

namespace Commit\Model;

/**
 * @Entity
 * @Table(name="commits")
 **/
class EntityDataMap
{
    /**
     * @var int
     * @Id
     * @Column(type="integer")
     * @GeneratedValue
     */
    protected $id;

    /**
     * @var int
     * @ManyToOne(targetEntity="Task\Model\EntityDataMap", inversedBy="commits")
     */
    protected $task;

    /**
     * @var int
     * @ManyToOne(targetEntity="User\Model\EntityDataMap")
     */
    protected $user;

    /**
     * @var string
     * @Column(type="text", nullable=true)
     */
    protected $text;

    /**
     * @var \DateTime
     * @Column(type="datetime")
     */
    protected $date;

    /**
     * @var array
     * @ManyToMany(targetEntity="File\Model\EntityDataMap")
     * @JoinTable(
     *     name="commit_attachments",
     *     joinColumns={
     *         @JoinColumn(name="commit_id", referencedColumnName="id")
     *     },
     *     inverseJoinColumns={
     *         @JoinColumn(name="file_id", referencedColumnName="id")
     *     }
     * )
     */
    protected $attachments;

    /**
     * @var int
     * @Column(type="integer", nullable=true)
     */
    protected $elapsed_time;
}