<?php

namespace Base;

use Common\Message\Controller\Controller as Message;

abstract class View implements ViewInterface
{
    /**
     * @var Message[]
     */
    protected $messages = array();
    
    protected $id;
    
    public function __construct()
    {
        $this->id = spl_object_hash($this);
    }

    /**
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return $this
     */
    public function cleanMessages ()
    {
        $this->messages = array();
        return $this;
    }

    /**
     * @param Message|string $message
     * @return $this
     */
    public function addMessage ($message)
    {
        if (!($message instanceof Message)) {
            $message = new Message($message);
        }
        $this->messages[] = $message;
        return $this;
    }

    /**
     * @return \Common\Message\Controller\Controller[]
     */
    public function getMessages()
    {
        return $this->messages;
    }

    /**
     * @param \Common\Message\Controller\Controller[] $messages
     * @return $this
     */
    public function setMessages($messages)
    {
        $this->messages = $messages;
        return $this;
    }

    /**
     * @return $this
     */
    public function showMessages ()
    {
        echo implode("", $this->messages);
        return $this;
    }
}
