<?php

namespace Base;


interface ViewInterface
{
    /**
     * render it's state
     * @return void
     */
    public function render();
}
