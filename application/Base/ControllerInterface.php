<?php

namespace Base;


interface ControllerInterface
{
    public function init();
}