<?php

namespace Base\Form\Controller;


interface ControllerInterface
{
    public function processPost();
    public function declareFormFieldMap();
}