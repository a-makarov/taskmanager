<?php

namespace Base\Form\Controller;

use Base\Controller as BaseController;
use Base\Form\Model\FormFieldMap;
use Base\Form\View\View as FormView;

/**
 * @method FormView getView()
 */
abstract class Controller 
    extends BaseController
    implements ControllerInterface
{
    /**
     * @var FormFieldMap
     */
    protected $formFieldMap;

    /**
     * @return FormFieldMap
     */
    public function getFormFieldMap()
    {
        return $this->formFieldMap;
    }
    
    public function __construct()
    {
        $this->formFieldMap = $this->declareFormFieldMap();
        parent::__construct();
    }
    
    public function init () {
        $this->getView()->setFormFieldMap($this->formFieldMap);
        parent::init();
    }

    /**
     * @return bool
     */
    public function needPostProcessing () {
        return $this->formFieldMap->needPostProcessing();
    }
}