<?php

namespace Base\Form\View;

use Base\Form\Model\FormFieldMap;
use Base\View as BaseView;
use Localization\Texts;

abstract class View extends BaseView
{
    /**
     * @var FormFieldMap
     */
    protected $formFieldMap;

    /**
     * @var string
     */
    protected $formClass;

    /**
     * @var string
     */
    protected $buttonText = Texts::TO_SUBMIT;

    /**
     * @return FormFieldMap
     */
    public function getFormFieldMap()
    {
        return $this->formFieldMap;
    }

    /**
     * @param FormFieldMap $formFieldMap
     * @return $this
     */
    public function setFormFieldMap(FormFieldMap $formFieldMap)
    {
        $this->formFieldMap = $formFieldMap;
        return $this;
    }

    /**
     * @return string
     */
    public function getFormClass()
    {
        return $this->formClass;
    }

    /**
     * @param string $formClass
     * @return $this
     */
    public function setFormClass($formClass)
    {
        $this->formClass = $formClass;
        return $this;
    }

    /**
     * @return string
     */
    public function getButtonText()
    {
        return $this->buttonText;
    }

    /**
     * @param string $buttonText
     * @return $this
     */
    public function setButtonText($buttonText)
    {
        $this->buttonText = $buttonText;
        return $this;
    }

    public function render()
    {
        include __DIR__ . "/assets/View.php";
    }
}