<?php
use Common\Tools;
use Base\Form\View\View;
if(!Tools::isViewAssetInRightContext(get_called_class(), View::class)) return;
/**
 * @var View $this
 */

$formFieldMap = $this->getFormFieldMap();
?>

<form class="<?=$this->formClass?>" method="post">
    <?php $this->showMessages()?>
    <input type="hidden" name="<?=$formFieldMap->getFormIdentifier()?>">
    <?foreach($formFieldMap as $field):
        /** @var Common\FieldView\Field $field */?>
        <div class="form-group">
            <?$field->render()?>
        </div>
    <?endforeach?>
    <div class="clearfix">
        <button type="submit" class="btn btn-success pull-right"><?=$this->getButtonText()?></button>
    </div>
</form>