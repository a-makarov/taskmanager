<?php

namespace Base\Form\Model;


use Common\FieldView\Field;
use Common\Tools;
use \IteratorAggregate;

abstract class FormFieldMap implements IteratorAggregate
{
    public function fillFromPostRequest ()
    {
        if (!$this->needPostProcessing()) {
            return;
        }
        foreach ($this as $field) {
            /** @var Field $field */
            $key = $field->getFieldName();
            $field->setValue($_POST[$key]);
        }
    }
    
    public function getIterator () {
        $fields = get_object_vars($this);
        foreach ($fields as $key => $field) {
            if (! $field instanceof Field) {
                unset($fields[$key]);
            }
        }
        return new \ArrayIterator($fields);
    }

    /**
     * @return bool
     */
    public function needPostProcessing () {
        $formId = $this->getFormIdentifier();
        return Tools::isPostRequest() && isset($_POST[$formId]);
    }
    
    public function getFormIdentifier()
    {
        return md5(get_called_class());
    }
}
