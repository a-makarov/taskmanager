<?php

namespace Base;


abstract class Controller implements ControllerInterface
{
    /**
     * @var View
     */
    protected $view;

    /**
     * @return View
     */
    abstract protected function declareViewInstance();

    public function __construct()
    {
        $this->view = $this->declareViewInstance();
    }

    /**
     * @return View
     */
    public function getView()
    {
        return $this->view;
    }

    public function init () {
        $this->getView()->render();
    }
}