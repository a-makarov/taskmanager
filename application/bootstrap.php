<?php
require_once __DIR__ . "/dump.php";
require_once __DIR__ . "/configuration.php";
require_once __DIR__ . "/../vendor/autoload.php";
require_once __DIR__ . "/doctrine_init.php";


global $application, $layout;

$application = new Main\Application();
$layout = new \Layout\Controller\Layout();
