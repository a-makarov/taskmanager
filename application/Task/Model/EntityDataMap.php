<?php

namespace Task\Model;

use Common\DataMap\Entity;
use DateTime;
use User\Model\EntityDataMap as UserEntity;

/**
 * @Entity
 * @Table(name="tasks")
 *
 * @method string    |$this title(string $value = null)
 * @method string    |$this body(string $value = null)
 * @method UserEntity|$this author(UserEntity $value = null)
 * @method DateTime  |$this createdDate(DateTime $value = null)
 **/
class EntityDataMap extends Entity
{
    /**
     * @var int
     * @Id
     * @Column(type="integer")
     * @GeneratedValue
     */
    protected $id;

    /**
     * @var int
     * @ManyToOne(targetEntity="Project\Model\EntityDataMap", inversedBy="tasks")
     */
    protected $project;

    /**
     * @var string
     * @Column(type="string")
     */
    protected $title;

    /**
     * @var string
     * @Column(type="text")
     */
    protected $body;

    /**
     * @var UserEntity
     * @ManyToOne(targetEntity="User\Model\EntityDataMap")
     */
    protected $author;

    /**
     * @var array
     * @ManyToMany(targetEntity="User\Model\EntityDataMap")
     * @JoinTable(
     *     name="task_performers",
     *     joinColumns={
     *         @JoinColumn(name="task_id", referencedColumnName="id")
     *     },
     *     inverseJoinColumns={
     *         @JoinColumn(name="user_id", referencedColumnName="id")
     *     }
     * )
     */
    protected $performers;

    /**
     * @var array
     * @ManyToMany(targetEntity="User\Model\EntityDataMap")
     * @JoinTable(
     *     name="task_controllers",
     *     joinColumns={
     *         @JoinColumn(name="task_id", referencedColumnName="id")
     *     },
     *     inverseJoinColumns={
     *         @JoinColumn(name="user_id", referencedColumnName="id")
     *     }
     * )
     */
    protected $controllers;

    /**
     * @var array
     * @ManyToMany(targetEntity="File\Model\EntityDataMap")
     * @JoinTable(
     *     name="task_attachments",
     *     joinColumns={
     *         @JoinColumn(name="task_id", referencedColumnName="id")
     *     },
     *     inverseJoinColumns={
     *         @JoinColumn(name="file_id", referencedColumnName="id")
     *     }
     * )
     */
    protected $attachments;

    /**
     * @var DateTime
     * @Column(type="datetime")
     */
    protected $created_date;

    /**
     * @var string
     * @OneToMany(targetEntity="\Commit\Model\EntityDataMap", mappedBy="task")
     */
    protected $commits;

    public function __construct()
    {
        $this->createdDate(new DateTime);
    }
}
