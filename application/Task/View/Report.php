<?php

namespace Task\View;


use Base\View as BaseView;

class Report extends BaseView
{
    public function render()
    {
        include __DIR__ . "/assets/Report.php";
    }
}