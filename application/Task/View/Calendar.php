<?php

namespace Task\View;


use Base\View as BaseView;

class Calendar extends BaseView
{
    public function render()
    {
        include __DIR__ . "/assets/Calendar.php";
    }
}