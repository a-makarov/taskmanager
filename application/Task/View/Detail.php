<?php

namespace Task\View;


use Base\View as BaseView;

class Detail extends BaseView
{
    public function render()
    {
        include __DIR__ . "/assets/Detail.php";
    }
}