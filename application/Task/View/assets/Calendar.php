<?php
use Common\Tools;
use Task\View\Calendar as View;

if (!Tools::isViewAssetInRightContext(get_called_class(), View::class)) return;
/**
 * @var View $this
 */
global $layout;
$layout->setTitle("Календарь для планирования рабочего времени");
?>

<div class="container">
    <div class="row">
        <div class="col-md-12">
            <table class="info-table">
                <tr>
                    <th>Название задачи</th>
                    <th>Затраченное время</th>
                    <th>Статус</th>
                    <th>Начата</th>
                    <th>Дедлайн</th>
                    <th>Цвет</th>
                </tr>
                <tr>
                    <td>Реализовать интеграцию с внешним платежным сервисом</td>
                    <td>12 часов 10 минут</td>
                    <td>Не завершена</td>
                    <td>10 июня</td>
                    <td class="text-danger">15 июня</td>
                    <td>
                        <div class="color-line green"></div>
                    </td>
                </tr>
                <tr>
                    <td>Интегрировать новую верстку главной страницы</td>
                    <td>4 часа 20 минут</td>
                    <td>Не завершена</td>
                    <td>14 июня</td>
                    <td>22 июня</td>
                    <td>
                        <div class="color-line yellow"></div>
                    </td>
                </tr>
                <tr>
                    <td>Произвести рефакторинг модуля авторизации</td>
                    <td>8 часов</td>
                    <td>Завершена</td>
                    <td>9 июня</td>
                    <td>10 июня</td>
                    <td>
                        <div class="color-line blue"></div>
                    </td>
                </tr>
                <tr>
                    <td>Реализовать калькулятор расчета доставки</td>
                    <td>26 часов 10 минут</td>
                    <td>Завершена</td>
                    <td>28 мая</td>
                    <td>7 июня</td>
                    <td>
                        <div class="color-line red"></div>
                    </td>
                </tr>
            </table>
        </div>
    </div>
    <div class="row">
        <div class="col-md-1">
            <div class="calendar-navigation glyphicon glyphicon-chevron-left"></div>
        </div>
        <div class="col-md-10">
            <h2 class="text-center">Июнь</h2>
            <table class="info-table calendar">
                <tr>
                    <th>Пн</th>
                    <th>Вт</th>
                    <th>Ср</th>
                    <th>Чт</th>
                    <th>Пт</th>
                    <th class="disabled">Сб</th>
                    <th class="disabled">Вс</th>
                </tr>
                <tr>
                    <td class="disabled">
                        <div class="task-level task-level-4 color-line red"></div>
                        30
                    </td>
                    <td class="disabled">
                        <div class="task-level task-level-4 color-line red"></div>
                        31
                    </td>
                    <td>
                        <div class="task-level task-level-4 color-line red"></div>
                        1
                    </td>
                    <td>
                        <div class="task-level task-level-4 color-line red"></div>
                        2
                    </td>
                    <td>
                        <div class="task-level task-level-4 color-line red"></div>
                        3
                    </td>
                    <td class="disabled">
                        <div class="task-level task-level-4 color-line red"></div>
                        4
                    </td>
                    <td class="disabled">
                        <div class="task-level task-level-4 color-line red"></div>
                        5
                    </td>
                </tr>
                <tr>
                    <td>
                        <div class="task-level task-level-4 color-line red"></div>
                        6
                    </td>
                    <td>
                        <div class="task-level task-level-4 color-line red"></div>
                        7
                    </td>
                    <td>8</td>
                    <td>
                        <div class="task-level task-level-3 color-line blue"></div>
                        9
                    </td>
                    <td>
                        <div class="task-level task-level-1 color-line green"></div>
                        <div class="task-level task-level-3 color-line blue"></div>
                        10
                    </td>
                    <td class="disabled">
                        <div class="task-level task-level-1 color-line green"></div>
                        11
                    </td>
                    <td class="disabled">
                        <div class="task-level task-level-1 color-line green"></div>
                        12
                    </td>
                </tr>
                <tr>
                    <td>
                        <div class="task-level task-level-1 color-line green"></div>
                        13
                    </td>
                    <td>
                        <div class="task-level task-level-1 color-line green"></div>
                        <div class="task-level task-level-2 color-line yellow"></div>
                        14
                    </td>
                    <td>
                        <div class="task-level task-level-1 color-line green"></div>
                        <div class="task-level task-level-2 color-line yellow"></div>
                        15
                    </td>
                    <td>
                        <div class="task-level task-level-2 color-line yellow"></div>
                        16
                    </td>
                    <td>
                        <div class="task-level task-level-2 color-line yellow"></div>
                        17
                    </td>
                    <td class="disabled">
                        <div class="task-level task-level-2 color-line yellow"></div>
                        18
                    </td>
                    <td class="disabled">
                        <div class="task-level task-level-2 color-line yellow"></div>
                        19
                    </td>
                </tr>
                <tr>
                    <td>
                        <div class="task-level task-level-2 color-line yellow"></div>
                        20
                    </td>
                    <td>
                        <div class="task-level task-level-2 color-line yellow"></div>
                        21
                    </td>
                    <td>
                        <div class="task-level task-level-2 color-line yellow"></div>
                        22
                    </td>
                    <td>23</td>
                    <td>24</td>
                    <td class="disabled">25</td>
                    <td class="disabled">26</td>
                </tr>
                <tr>
                    <td>27</td>
                    <td>28</td>
                    <td>29</td>
                    <td>30</td>
                    <td class="disabled">1</td>
                    <td class="disabled">2</td>
                    <td class="disabled">3</td>
                </tr>
            </table>
        </div>
        <div class="col-md-1">
            <div class="calendar-navigation glyphicon glyphicon-chevron-right"></div>
        </div>
    </div>
</div>
