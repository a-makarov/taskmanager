<?php
use Common\Tools;
use Task\View\Create as View;
if(!Tools::isViewAssetInRightContext(get_called_class(), View::class)) return;
/**
 * @var View $this
 */
global $layout;
$layout->setTitle("Создание задачи");
?>

<div class="container">
    <div class="col-md-6">
        <form>
            <div class="form-group">
                <label for="name">Название</label>
                <input type="text" id="name" class="form-control">
            </div>
            <div class="form-group">
                <label for="name">Описание</label>
                <textarea id="name" class="form-control" rows="7"></textarea>
            </div>
            <div class="form-group">
                <label for="date">Дата сдачи</label>
                <input type="date" id="date" class="form-control">
            </div>
            <div class="form-group">
                <label for="file">Вложения</label>
                <input type="file" id="file" class="form-control" style="margin-bottom: 7px">
                <input type="file" id="file" class="form-control" style="margin-bottom: 7px">
                <button class="btn btn-xs btn-default">Еще файл</button>
            </div>
        </form>
    </div>
</div>

<div class="container">
    <div class="col-md-4">
        <h2>Исполнители</h2>
        <ul class="personal">
            <li>Александр Пушкин</li>
            <li>Василий Пупкин</li>
        </ul>
        <button class="btn btn-default">Добавить исполнителя</button>
    </div>
    <div class="col-md-4">
        <h2>Контролёры</h2>
        <ul class="personal">
            <li>Иван Никитин</li>
        </ul>
        <button class="btn btn-default">Добавить контролёра</button>
    </div>
    <div class="col-md-4">
        <h2>Наблюдатели</h2>
        Наблюдателей пока нет
         <br>
         <br>
        <button class="btn btn-default">Добавить наблюдателя</button>
    </div>
</div>
<br>
<br>

<div class="container">
    <div class="col-md-12">
        <h2>Привелегии для пользователей <small>не являющихся контрибьюторами задачи</small></h2>
        <table class="info-table">
            <tr>
                <th></th>
                <th>Администратор</th>
                <th>Контролёр</th>
                <th>Исполнитель</th>
            </tr>
            <tr>
                <td>Просмотр задачи</td>
                <td class="disabled"><input type="checkbox" checked disabled></td>
                <td><input type="checkbox" checked></td>
                <td><input type="checkbox" checked></td>
            </tr>
            <tr>
                <td>Управление задачей</td>
                <td class="disabled"><input type="checkbox" checked disabled></td>
                <td><input type="checkbox"></td>
                <td><input type="checkbox"></td>
            </tr>
            <tr>
                <td>Добавление коммитов</td>
                <td class="disabled"><input type="checkbox" checked disabled></td>
                <td><input type="checkbox"></td>
                <td><input type="checkbox"></td>
            </tr>
        </table>
        <br>
        <button type="submit" class="btn btn-success">Сохранить</button>
    </div>
</div>