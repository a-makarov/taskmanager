<?php
use Common\Tools;
use Task\View\Report as View;
if(!Tools::isViewAssetInRightContext(get_called_class(), View::class)) return;
/**
 * @var View $this
 */
global $layout;
$layout->setTitle("Отчет о трудовых и финансовых затратах");
?>

<div class="container">
    <div class="panel panel-default">
        <div class="panel-heading">
            <h2>Временной фильтр</h2>
        </div>
        <div class="panel-body">
            <div class="form-group">
                <label for="date">Начальная дата</label>
                <input type="date" id="date" class="form-control" value="2016-06-01">
            </div>
            <div class="form-group">
                <label for="date">Конечная дата</label>
                <input type="date" id="date" class="form-control" value="2016-06-30">
            </div>
            <button type="submit" class="btn btn-info">Построить отчет</button>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <table class="info-table" width="100%">
                <tr>
                    <th>Имя пользователя</th>
                    <th>Количество задач</th>
                    <th>Из них завершено</th>
                    <th>Затраченное время</th>
                    <th>Ставка часа</th>
                    <th>Премиальные</th>
                </tr>
                <tr class="disabled">
                    <td>Александр Пушкин</td>
                    <td>4</td>
                    <td>1</td>
                    <td>48 часов 30 минут</td>
                    <td>200</td>
                    <td>9&nbsp;700</td>
                </tr>
                <tr class="report-detail">
                    <td colspan="6">
                        <table>
                            <tr>
                                <th>Название задачи</th>
                                <th>Затраченное время</th>
                                <th>Статус</th>
                                <th>Дедлайн</th>
                            </tr>
                            <tr>
                                <td>Реализовать интеграцию с внешним платежным сервисом</td>
                                <td>10 часов</td>
                                <td>Не завершена</td>
                                <td class="text-danger">15 июня</td>
                            </tr>
                            <tr>
                                <td>Интегрировать новую верстку главной страницы</td>
                                <td>4 часа 20 минут</td>
                                <td>Не завершена</td>
                                <td>22 июня</td>
                            </tr>
                            <tr>
                                <td>Произвести рефакторинг модуля авторизации</td>
                                <td>8 часов</td>
                                <td>Завершена</td>
                                <td>10 июня</td>
                            </tr>
                            <tr>
                                <td>Реализовать калькулятор расчета доставки</td>
                                <td>26 часов 10 минут</td>
                                <td>Завершена</td>
                                <td>7 июня</td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>Василий Пупкин</td>
                    <td>1</td>
                    <td>0</td>
                    <td>0</td>
                    <td>200</td>
                    <td>0</td>
                </tr>
                <tr>
                    <td>Олег Пирожков</td>
                    <td>3</td>
                    <td>1</td>
                    <td>4 часа 10 минут</td>
                    <td>150</td>
                    <td>615</td>
                </tr>
            </table>
        </div>
    </div>
</div>