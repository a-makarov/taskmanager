<?php
use Common\Tools;
use Task\View\Detail as View;
if(!Tools::isViewAssetInRightContext(get_called_class(), View::class)) return;
/**
 * @var View $this
 */
global $layout;
$layout->setTitle("Создание задачи");
?>

<div class="container">
    <div class="panel panel-default">
        <div class="panel-heading">
            <h2>Реализовать интеграцию с внешним платежным сервисом</h2>
        </div>
        <div class="panel-body">

            <div class="row">
                <div class="col-md-12">
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam et tincidunt metus. Morbi sed pretium risus, nec tempus nibh. Suspendisse potenti. Curabitur lectus risus, placerat eu libero ac, blandit bibendum magna. Quisque ac purus rutrum, bibendum urna et, hendrerit metus. Cras mattis augue sit amet lectus sagittis, a dignissim erat interdum. Nunc vitae suscipit justo, vitae fringilla ante. Mauris ultricies porta leo, nec pretium leo ullamcorper feugiat. Sed ut neque massa. Pellentesque vel lectus ut neque tristique pharetra a ac tortor. Nullam a erat in justo tempor vestibulum. Integer interdum, arcu id auctor dictum, augue velit accumsan quam, vitae semper dui enim eget orci. Sed pharetra ut purus id cursus. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos.</p>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4">
                    <h3>Исполнители</h3>
                    <ul class="personal">
                        <li>Александр Пушкин</li>
                        <li>Василий Пупкин</li>
                    </ul>
                </div>
                <div class="col-md-4">
                    <h3>Контролёры</h3>
                    <ul class="personal">
                        <li>Олег Пирожков</li>
                        <li class="text-success">Иван Никитин <span class="approved glyphicon glyphicon-ok"></span></li>
                    </ul>
                </div>
                <div class="col-md-4">
                    <h3>Наблюдатели</h3>
                    Наблюдателей нет
                </div>
            </div>
            <br>
            <div class="row">
                <div class="col-md-4">
                    Затраченное время: <span class="text-info">12 часов 10 минут</span>
                </div>
                <div class="col-md-8">
                    Дедлайн: <span class="text-danger">15 июня</span>
                </div>
            </div>
            <br>
            <div class="row">
                <div class="col-md-12">
                    <h4>Вложения</h4>
                    <ul class="list-group">
                        <li class="list-group-item">
                            <a href="#">Техническое задание</a>
                        </li>
                        <li class="list-group-item">
                            <a href="#">Screenshot from 2016-06-14 22-24-48</a>
                        </li>
                    </ul>
                </div>
            </div>
            <br>
            <div class="row">
                <div class="col-md-12 clearfix">
                    <button class="btn btn-default pull-right">Редактировать задачу</button>
                </div>
            </div>
        </div>
    </div>
    
    <div class="panel panel-default commit">
        <div class="panel-heading">
            Александр Пушкин
            <div class="date pull-right">11 июня 18:50</div>
        </div>
        <div class="panel-body">
            Поковырялся в API сервиса
        </div>
        <div class="panel-footer clearfix">
            Вложения: <a href="#">API.pdf</a>
            <div class="pull-right">Затраченное время: <span class="text-info">6 часов</span></div>
        </div>
    </div>
    <div class="panel panel-default commit">
        <div class="panel-heading">
            Александр Пушкин
            <div class="date pull-right">12 июня 15:12</div>
        </div>
        <div class="panel-body">
            Поплясал с бубном, теперь задача готова
        </div>
        <div class="panel-footer clearfix">
            <div class="pull-right">Затраченное время: <span class="text-info">4 часа</span></div>
        </div>
    </div>
    <div class="panel panel-default commit">
        <div class="panel-heading">
            Олег Пирожков
            <div class="date pull-right">12 июня 16:07</div>
        </div>
        <div class="panel-body">
            Шапку нужно по-ярче. И поиграйтесь со шрифтами. Не для похоронного бюро сайт делаем.
        </div>
        <div class="panel-footer clearfix">
            <div class="pull-right">Затраченное время: <span class="text-info">10 минут</span></div>
        </div>
    </div>
    <div class="panel panel-default commit">
        <div class="panel-heading">
            Александр Пушкин
            <div class="date pull-right">13 июня 14:02</div>
        </div>
        <div class="panel-body">
            Сделал правки Олега
        </div>
        <div class="panel-footer clearfix">
            <div class="pull-right">Затраченное время: <span class="text-info">2 часа</span></div>
        </div>
    </div>
    <div class="panel panel-default commit">
        <div class="panel-heading">
            Оставить коммит
        </div>
        <div class="panel-body">
            <div class="form-group">
                <label for="name">Описание</label>
                <textarea id="name" class="form-control" rows="7"></textarea>
            </div>
            <div class="form-group">
                <label for="name">Затраченное время</label>
                <input type="time" id="name" class="form-control">
            </div>
            <div class="form-group">
                <label for="file">Вложения</label>
                <input type="file" id="file" class="form-control" style="margin-bottom: 7px">
                <button class="btn btn-xs btn-default">Еще файл</button>
            </div>
            <div class="clearfix">
                <button type="submit" class="btn btn-success pull-right">Отправить</button>
            </div>
        </div>
    </div>
</div>
