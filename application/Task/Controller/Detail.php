<?php

namespace Task\Controller;

use Task\View\Detail as View;
use Base\Controller as BaseController;

class Detail extends BaseController
{
    public function declareViewInstance()
    {
        return new View();
    }
}