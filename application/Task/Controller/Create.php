<?php

namespace Task\Controller;

use Task\View\Create as View;
use Base\Controller as BaseController;

class Create extends BaseController
{
    public function declareViewInstance()
    {
        return new View();
    }
}