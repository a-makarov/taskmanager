<?php

namespace Task\Controller;

use Task\View\Report as View;
use Base\Controller as BaseController;

class Report extends BaseController
{
    public function declareViewInstance()
    {
        return new View();
    }
}