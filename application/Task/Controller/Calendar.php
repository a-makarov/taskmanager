<?php

namespace Task\Controller;

use Task\View\Calendar as View;
use Base\Controller as BaseController;

class Calendar extends BaseController
{
    public function declareViewInstance()
    {
        return new View();
    }
}