<?php

namespace Project\Model;

use Common\DataMap\Entity;
use User\Model\EntityDataMap as UserEntity;

/**
 * @Entity
 * @Table(name="projects")
 *
 * @method int id()
 * @method string    |$this name(string $value = null)
 * @method string    |$this description(string $value = null)
 * @method UserEntity|$this owner(UserEntity $value = null)
 * @method array roles()
 **/
class EntityDataMap extends Entity
{
    /**
     * @var int
     * @Id
     * @Column(type="integer")
     * @GeneratedValue
     */
    protected $id;

    /**
     * @var string
     * @Column(type="string")
     */
    protected $name;

    /**
     * @var string
     * @Column(type="text")
     */
    protected $description;

    /**
     * @var UserEntity
     * @ManyToOne(targetEntity="User\Model\EntityDataMap")
     */
    protected $owner;

    /**
     * @var array
     * @OneToMany(targetEntity="Role\Model\EntityDataMap", mappedBy="project")
     */
    protected $roles;

    /**
     * @var string
     * @OneToMany(targetEntity="\Task\Model\EntityDataMap", mappedBy="project")
     */
    protected $tasks;
}
