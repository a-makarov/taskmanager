<?php

namespace Project\View;


use Base\View as BaseView;

class Create extends BaseView
{
    public function render()
    {
        include __DIR__ . "/assets/Create.php";
    }
}