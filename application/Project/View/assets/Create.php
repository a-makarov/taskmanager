<?php
use Common\Tools;
use Project\View\Create as View;
if(!Tools::isViewAssetInRightContext(get_called_class(), View::class)) return;
/**
 * @var View $this
 */
global $layout;
$layout->setTitle("Создание проекта");
?>

<div class="container">
    <div class="col-md-6">
        <form>
            <div class="form-group">
                <label for="name">Название</label>
                <input type="text" id="name" class="form-control">
            </div>
            <div class="form-group">
                <label for="name">Описание</label>
                <textarea id="name" class="form-control" rows="7"></textarea>
            </div>
            <div class="form-group">
                <label><input type="checkbox"> Многопользовательский режим</label>
            </div>
        </form>
    </div>
</div>

<div class="container">
    <div class="col-md-12">
        <h2>Управление ролями пользователей</h2>
        <table class="info-table">
            <tr>
                <th></th>
                <th>Управление ролями</th>
                <th>Добавление пользователей</th>
                <th>Создание задач</th>
                <th>Контроль исполнения задач</th>
                <th>Исполнение задач</th>
                <th></th>
            </tr>
            <tr class="disabled">
                <td>Администратор</td>
                <td><input type="checkbox" checked disabled></td>
                <td><input type="checkbox" checked disabled></td>
                <td><input type="checkbox" checked disabled></td>
                <td><input type="checkbox" checked disabled></td>
                <td><input type="checkbox" checked disabled></td>
                <td></td>
            </tr>
            <tr>
                <td>Контролёр</td>
                <td><input type="checkbox"></td>
                <td><input type="checkbox"></td>
                <td><input type="checkbox" checked></td>
                <td><input type="checkbox" checked></td>
                <td><input type="checkbox" checked></td>
                <td class="remove-cell"><span class="btn-remove glyphicon glyphicon-remove"></span> Удалить</td>
            </tr>
            <tr>
                <td>Исполнитель</td>
                <td><input type="checkbox"></td>
                <td><input type="checkbox"></td>
                <td><input type="checkbox"></td>
                <td><input type="checkbox"></td>
                <td><input type="checkbox" checked></td>
                <td class="remove-cell"><span class="btn-remove glyphicon glyphicon-remove"></span> Удалить</td>
            </tr>
        </table>
        <button class="btn btn-default">Добавить роль</button>
    </div>
</div>
<br>
<br>

<div class="container">
    <div class="col-md-12">
        <h2>Добавление пользователей</h2>
        <table class="info-table">
            <tr>
                <th></th>
                <th>Администратор</th>
                <th>Котролёр</th>
                <th>Исполнитель</th>
                <th></th>
            </tr>
            <tr class="disabled">
                <td>test@example.com (Вы)</td>
                <td><input type="checkbox" checked disabled></td>
                <td><input type="checkbox"></td>
                <td><input type="checkbox"></td>
                <td></td>
            </tr>
            <tr>
                <td>vasya.pupkin@gmail.com</td>
                <td><input type="checkbox"></td>
                <td><input type="checkbox"></td>
                <td><input type="checkbox" checked></td>
                <td class="remove-cell"><span class="btn-remove glyphicon glyphicon-remove"></span> Удалить</td>
            </tr>
            <tr>
                <td>super.manager@gmail.com</td>
                <td><input type="checkbox"></td>
                <td><input type="checkbox" checked></td>
                <td><input type="checkbox"></td>
                <td class="remove-cell"><span class="btn-remove glyphicon glyphicon-remove"></span> Удалить</td>
            </tr>
        </table>
        <button class="btn btn-default">Добавить пользователя</button>
        <br>
        <br>
        <br>
        <button type="submit" class="btn btn-success">Сохранить</button>
    </div>
</div>
