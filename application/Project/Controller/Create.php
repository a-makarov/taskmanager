<?php

namespace Project\Controller;

use Project\View\Create as View;
use Base\Controller as BaseController;

class Create extends BaseController
{
    public function declareViewInstance()
    {
        return new View();
    }
}