<?php

namespace File\Model;

use Common\DataMap\Entity;
use DateTime;

/**
 * @Entity
 * @Table(name="files")
 *
 * @method int id()
 * @method string  |$this path(string $value = null)
 * @method string  |$this type(string $value = null)
 * @method string  |$this realName(string $value = null)
 * @method string  |$this size(string $value = null)
 * @method DateTime|$this uploadedDate(DateTime $value = null)
 **/
class EntityDataMap extends Entity
{
    /**
     * @var int
     * @Id
     * @Column(type="integer")
     * @GeneratedValue
     */
    protected $id;

    /**
     * @var string
     * @Column(type="string")
     */
    protected $path;

    /**
     * @var string
     * @Column(type="string")
     */
    protected $type;

    /**
     * @var string
     * @Column(type="string")
     */
    protected $real_name;

    /**
     * @var string
     * @Column(type="string")
     */
    protected $size;

    /**
     * @var DateTime
     * @Column(type="datetime")
     */
    protected $uploaded_date;
}
