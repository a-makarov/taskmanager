<?php

namespace Layout\Controller;

use Layout\Controller\DeferredContent\ControlTrait as DeferredContentControlTrait;
use Layout\View\Footer as FooterView;
use Layout\View\Header as HeaderView;

class Layout
{
    use DeferredContentControlTrait;

    /**
     * @var HeaderView
     */
    protected $headerView;

    /**
     * @var FooterView
     */
    protected $footerView;

    /**
     * Layout constructor
     */
    public function __construct()
    {
        $this->headerView = new HeaderView();
        $this->footerView = new FooterView();
        $this->setTitle("Task manager");
    }

    /**
     * @return $this
     */
    public function init()
    {
        ob_start(array($this, "modifyBuffer"));
        $this->headerView->render();
        return $this;
    }

    /**
     * @return $this
     */
    public function close()
    {
        $this->footerView->render();
        $buffer = $this->cleanBuffer();
        echo $this->modifyBuffer($buffer);
        return $this;
    }

    /**
     * @return string buffer
     */
    public function cleanBuffer()
    {
        $buffer = "";
        while ($subContent = ob_get_clean()) {
            $buffer .= $subContent;
        }
        return $buffer;
    }

    /**
     * @param string $title
     * @return $this
     */
    public function setTitle ($title)
    {
        return $this->setDeferredContentController("title", $title);
    }

    /**
     * @return $this
     */
    public function showTitle ()
    {
        return $this->showDeferredContent("title");
    }

    /**
     * @param string $class
     * @return $this
     */
    public function setAdditionalClassForMain ($class)
    {
        return $this->setDeferredContentController("main-additional-class", $class);
    }

    /**
     * @return $this
     */
    public function showAdditionalClassForMain ()
    {
        return $this->showDeferredContent("main-additional-class");
    }

    /**
     * @param string &$buffer
     * @return string $buffer
     */
    protected function modifyBuffer($buffer)
    {
        return $this->placeDeferredContent($buffer);
    }
}
