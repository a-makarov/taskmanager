<?php

namespace Layout\Controller\DeferredContent;

trait ControlTrait
{
    /**
     * collect of Deferred functions
     * @var Controller
     */
    protected $deferredContentController;

    /**
     * @return Controller
     */
    public function getDeferredContentController ()
    {
        if (is_null($this->deferredContentController)) {
            $this->deferredContentController = new Controller();
        }
        return $this->deferredContentController;
    }

    /**
     * @param callable $callback
     * @param string $key
     * @return string
     */
    public function addDeferredFunction($callback, $key)
    {
        return $this->getDeferredContentController()->add($callback, $key);
    }

    /**
     * @param callable $callback
     * @param string $key
     * @return string
     */
    public function setDeferredFunction($callback, $key)
    {
        return $this->getDeferredContentController()->set($callback, $key);
    }

    /**
     * @param string $key
     * @return $this
     */
    public function showDeferredContent($key)
    {
        echo $this->getDeferredContentController()->getReplacement($key);
        return $this;
    }

    /**
     * @param string $key
     * @param string $content
     * @return $this
     */
    public function addDeferredContent($key, $content)
    {
        $this->getDeferredContentController()->addContent($key, $content);
        return $this;
    }

    /**
     * @param string $key
     * @param string $content
     * @return $this
     */
    public function setDeferredContentController($key, $content)
    {
        $this->getDeferredContentController()->setContent($key, $content);
        return $this;
    }

    /**
     * @param string $buffer
     * @return string $buffer
     */
    public function placeDeferredContent($buffer)
    {
        return $this->getDeferredContentController()->modifyBuffer($buffer);
    }
}