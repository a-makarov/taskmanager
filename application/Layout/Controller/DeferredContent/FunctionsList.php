<?php

namespace Layout\Controller\DeferredContent;

class FunctionsList
{
    /**
     * @var array
     */
    protected $arFunctions = array();

    /**
     * @var string
     */
    protected $token;

    /**
     * @param string $token
     */
    public function __construct($token)
    {
        $this->token = $token;
    }

    /**
     * @return string
     */
    public function getToken ()
    {
        return $this->token;
    }

    /**
     * @return $this
     */
    public function clean ()
    {
        $this->arFunctions = array();
        return $this;
    }

    /**
     * @param callable $function
     * @return $this
     */
    public function addFunction ($function)
    {
        $this->arFunctions[] = $function;
        return $this;
    }

    /**
     * @param callable $function
     */
    public function setFunction ($function)
    {
        $this->clean()->addFunction($function);
    }

    /**
     * @return string
     */
    public function execute ()
    {
        $content = "";
        foreach ($this->arFunctions as $function) {
            $content .= $function();
        }
        return $content;
    }
}