<?php

namespace Layout\Controller\DeferredContent;

use Common\Alphabet;

class Controller
{
    const TOKEN_LENGTH = 64;

    const GENERATED_KEY_LENGTH = 64;

    /**
     * @var FunctionsList[]
     */
    protected $arFunctionsLists = array();

    /**
     * @var string
     */
    protected $objectId;

    /**
     * DelayedContent constructor
     */
    public function __construct()
    {
        $this->objectId = spl_object_hash($this);
    }

    /**
     * @param $key
     * @return void
     */
    public function cleanFunctionList($key)
    {
        $functionList = $this->getFunctionList($key);
        if (!is_null($functionList)) {
            $functionList->clean();
        }
    }

    /**
     * @param callable $callback
     * @param string $key
     * @return string
     */
    public function add($callback, $key = "")
    {
        $this->formatFunctionsListKey($key);
        $functionsList = $this->initFunctionsList($key);
        $functionsList->addFunction($callback);
        return $key;
    }

    /**
     * @param callable $callback
     * @param string $key
     * @return string
     */
    public function set($callback, $key = "")
    {
        $this->cleanFunctionList($key);
        return $this->add($callback, $key);
    }

    /**
     * @param string $key
     * @param string $content
     * @return void
     */
    public function addContent($key, $content)
    {
        $callback = $this->wrapContentInClosure($content);
        $this->add($callback, $key);
    }

    /**
     * @param string $key
     * @param string $content
     * @return void
     */
    public function setContent($key, $content)
    {
        $callback = $this->wrapContentInClosure($content);
        $this->set($callback, $key);
    }

    /**
     * @param mixed $content
     * @return \Closure
     */
    protected function wrapContentInClosure ($content)
    {
        return function () use ($content) {
            return $content;
        };
    }

    /**
     * @param $key
     * @return FunctionsList
     */
    protected function initFunctionsList ($key)
    {
        if (isset($this->arFunctionsLists[$key])) {
            return $this->arFunctionsLists[$key];
        }
        $token = $this->generateDelayedContentToken();
        $this->arFunctionsLists[$key] = new FunctionsList($token);
        return $this->arFunctionsLists[$key];
    }

    /**
     * @param &$key
     */
    protected function formatFunctionsListKey (&$key)
    {
        $key = strval($key);
        if (empty($key)) {
            $key = $this->generateFunctionsListKey();
        }
    }

    /**
     * @return string
     */
    protected function generateFunctionsListKey ()
    {
        do {
            $key = Alphabet::generateRandomString(self::GENERATED_KEY_LENGTH);
        } while(isset($this->arFunctionsLists[$key]));
        return $key;
    }

    /**
     * @param string $key
     * @return string
     */
    public function getReplacement($key)
    {
        $functionsList = $this->initFunctionsList($key);
        $token = $functionsList->getToken();
        $objectId = $this->objectId;
        return "{delayed_content:$objectId:$token}";
    }

    /**
     * @return string
     */
    protected function generateDelayedContentToken()
    {
        do {
            $token = Alphabet::generateRandomString(self::TOKEN_LENGTH);
        } while ($this->existFunctionListWithToken($token));
        return $token;
    }

    /**
     * @param string $token
     * @return FunctionsList|null
     */
    public function getFunctionListByToken($token)
    {
        foreach ($this->arFunctionsLists as $functionList) {
            if ($token == $functionList->getToken()) {
                return $functionList;
            }
        }
        return null;
    }

    /**
     * @param string $key
     * @return FunctionsList|null
     */
    public function getFunctionList($key)
    {
        return isset($this->arFunctionsLists[$key]) ? $this->arFunctionsLists[$key] : null;
    }

    /**
     * @param string $token
     * @return bool
     */
    public function existFunctionListWithToken($token)
    {
        return !is_null($this->getFunctionListByToken($token));
    }

    /**
     * @param string &$buffer
     * @return string $buffer
     */
    public function modifyBuffer($buffer)
    {
        foreach ($this->arFunctionsLists as $token => $functionsList) {
            $replacement = $this->getReplacement($token);
            $content = $functionsList->execute();
            $buffer = str_replace($replacement, $content, $buffer);
        }
        return $buffer;
    }
}