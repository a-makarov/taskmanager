<?php

namespace Layout\View;

use Base\View as BaseView;

class Footer extends BaseView
{
    public function render()
    {
        include __DIR__ . "/assets/Footer.php";
    }
}
