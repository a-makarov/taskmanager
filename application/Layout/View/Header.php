<?php

namespace Layout\View;

use Base\View as BaseView;

class Header extends BaseView
{
    public function render()
    {
        include __DIR__ . "/assets/Header.php";
    }
}
