<?php

namespace Layout\View;


use Base\View as BaseView;

class Menu extends BaseView
{
    protected $items = array(
        "projects_list" => "Проекты",
        "tasks_list" => "Задачи",
        "calendar" => "Календарь",
        "report" => "Отчет",
        "profile" => "Личный кабинет",
    );
    
    protected function getLinks ()
    {
        global $application;
        $links = array();
        $urlGenerator = $application->getUrlGenerator();
        $request = $application->getHttpFoundationRequest();
        $path = $request->getPathInfo();
        foreach ($this->items as $name => $text) {
            $link = $urlGenerator->generate($name);
            $links[] = array(
                "link" => $link,
                "text" => $text,
                "active" => $link == $path
            );
        }
        return $links;
    }

    public function render()
    {
        include __DIR__ . "/assets/Menu.php";
    }
}