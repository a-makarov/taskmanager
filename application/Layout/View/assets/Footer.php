<?php
use Common\Tools;
use Layout\View\Footer as View;
if(!Tools::isViewAssetInRightContext(get_called_class(), View::class)) return;
/**
 * @var View $this
 */
?>
</main>
<script src="/public/js/main.js"></script>
</body>
</html>
