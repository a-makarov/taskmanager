<?php
use Common\Tools;
use Layout\View\Header as View;
use Layout\View\Menu;
if(!Tools::isViewAssetInRightContext(get_called_class(), View::class)) return;
/**
 * @var View $this
 */

global $layout;

$menu = new Menu()
?>
<!doctype html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?php $layout->showTitle()?></title>
    <link rel="stylesheet" href="/public/styles/main.css">
</head>
<body>
<main class="<?$layout->showAdditionalClassForMain()?>">
<!--    --><?//$menu->render()?>
<!--    <div class="container">-->
<!--        <div class="col-md-12">-->
<!--            <h1>--><?php //$layout->showTitle()?><!--</h1>-->
<!--        </div>-->
<!--    </div>-->