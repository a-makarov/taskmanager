<?php
use Common\Tools;
use Layout\View\Menu as View;
if(!Tools::isViewAssetInRightContext(get_called_class(), View::class)) return;
/**
 * @var View $this
 */
?>
<header>
    <div class="container">
        <ul class="menu">
            <?foreach($this->getLinks() as $link):?>
                <li>
                    <a href="<?=$link["link"]?>" class="<?=($link["active"] ? "active" : "")?>">
                        <?=$link["text"]?>
                    </a>
                </li>
            <?endforeach;?>
        </ul>
    </div>
</header>