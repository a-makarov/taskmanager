<?php

namespace Main;

use Auth\Model\Model as AuthModel;
use Auth\Controller\GettingStarted;
use Base\Controller as BaseController;
use ServerError\Controller\Controller as ServerErrorController;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\HttpFoundation\Request as HttpFoundationRequest;
use Symfony\Component\Routing\Exception\ResourceNotFoundException;
use Symfony\Component\Routing\Generator\UrlGenerator;
use Symfony\Component\Routing\Loader\YamlFileLoader;
use Symfony\Component\Routing\Matcher\UrlMatcher;
use Symfony\Component\Routing\RequestContext;
use Symfony\Component\Routing\RouteCollection;

class Application
{
    /**
     * @var RouteCollection
     */
    protected $routeCollection;

    /**
     * @var UrlGenerator
     */
    protected $urlGenerator;

    /**
     * @var RequestContext
     */
    protected $requestContext;

    /**
     * @var HTTPFoundationRequest
     */
    protected $httpFoundationRequest;

    /**
     * @var UrlMatcher
     */
    protected $urlMatcher;

    public function __construct()
    {
        $this->initRouteCollection();
        $this->initHttpFoundationRequest();
        $this->initRequestContext();
        $this->initUrlGenerator();
        $this->initUrlMatcher();
    }

    /**
     * init routing
     * @return BaseController
     */
    public function route()
    {
        if (!AuthModel::authenticate()) {
            $gettingStarted = new GettingStarted();
            $gettingStarted->init();
            return $gettingStarted;
        }

        try {
            $action = $this->matchAction();
        } catch (ResourceNotFoundException $exception) {
            $serverError = new ServerErrorController();
            $serverError->init();
            return $serverError;
        }

        $controller = new $action[0];
        $method = $action[1];
        $controller->$method();

        return $controller;
    }
    
    protected function initRouteCollection () 
    {
        $locator = new FileLocator(DOCUMENT_ROOT);
        $loader = new YamlFileLoader($locator);
        $this->routeCollection = $routeCollection = $loader->load(DOCUMENT_ROOT . "/routes.yml");
    }
    
    protected function initHttpFoundationRequest ()
    {
        $this->httpFoundationRequest = HttpFoundationRequest::createFromGlobals();
    }
    
    protected function initRequestContext () 
    {
        $this->requestContext = $requestContext = new RequestContext();
        $requestContext->fromRequest($this->getHttpFoundationRequest());
    }

    protected function initUrlGenerator ()
    {
        $this->urlGenerator = new UrlGenerator($this->getRouteCollection(), $this->getRequestContext());
    }

    protected function initUrlMatcher ()
    {
        $this->urlMatcher = new UrlMatcher($this->getRouteCollection(), $this->getRequestContext());
    }

    protected function matchAction ()
    {
        $match = $this->getUrlMatcher()->matchRequest($this->getHttpFoundationRequest());
        return explode("::", $match["_controller"]);
    }

    /**
     * @return RouteCollection
     */
    public function getRouteCollection()
    {
        return $this->routeCollection;
    }

    /**
     * @return UrlGenerator
     */
    public function getUrlGenerator()
    {
        return $this->urlGenerator;
    }

    /**
     * @return RequestContext
     */
    public function getRequestContext()
    {
        return $this->requestContext;
    }

    /**
     * @return HTTPFoundationRequest
     */
    public function getHttpFoundationRequest()
    {
        return $this->httpFoundationRequest;
    }

    /**
     * @return UrlMatcher
     */
    public function getUrlMatcher()
    {
        return $this->urlMatcher;
    }
}
