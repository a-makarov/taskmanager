<?php

namespace Common\DataMap;

abstract class Entity
{
    /**
     * creates methods for save data to it's properties
     * determined property name by method $this->determinePropertyName($name).
     *
     * @param $name
     * @param $arguments
     * @return $this
     * @throws \Exception
     */
    public function __call($name, $arguments)
    {
        $property = $this->determinePropertyName($name);
        if (is_null($property)) {
            trigger_error("Call to undefined function $name", E_USER_ERROR);
        }

        if (empty($arguments)) {
            return $this->$property;
        }
        $this->$property = array_shift($arguments);
        return $this;
    }

    /**
     * determines property name from camelCase method name format
     * for camelCase and lower_case formats of property names
     * for example calling of $this->propertyName() may determined
     * it's property $this->property_name.
     *
     * @param $name
     * @return mixed|null|string
     */
    public function determinePropertyName ($name)
    {
        if (property_exists($this, $name)) {
            return $name;
        }
        $property = preg_replace("/([A-Z])/", "_\$1", $name);
        $property = mb_strtolower($property);
        if (property_exists($this, $property)) {
            return $property;
        }
        return null;
    }

    /**
     * persist&flush entity to database.
     * @return $this
     */
    public function save ()
    {
        global $EM;
        $EM->persist($this);
        $EM->flush();
        return $this;
    }
}
