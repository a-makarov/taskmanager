<?php
use Common\Tools;
use Common\FieldView\Textarea as View;
if(!Tools::isViewAssetInRightContext(get_called_class(), View::class)) return;
/**
 * @var View $this
 */
?>

<?parent::render()?>
<textarea
    class="form-control <?=$this->getAdditionalClass()?>"
    id="<?=$this->getId()?>"
    name="<?=$this->getFieldName()?>"
><?=$this->getValue()?></textarea>