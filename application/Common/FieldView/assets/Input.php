<?php
use Common\Tools;
use Common\FieldView\Input as View;
if(!Tools::isViewAssetInRightContext(get_called_class(), View::class)) return;
/**
 * @var View $this
 */
?>

<?parent::render()?>
<input
    class="form-control <?=$this->getAdditionalClass()?>"
    id="<?=$this->getId()?>"
    type="<?=$this->getInputType()?>"
    name="<?=$this->getFieldName()?>"
    value="<?=$this->getValue()?>"
>