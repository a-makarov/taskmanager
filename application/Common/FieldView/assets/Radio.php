<?php
use Common\Tools;
use Common\FieldView\Radio as View;
if(!Tools::isViewAssetInRightContext(get_called_class(), View::class)) return;
/**
 * @var View $this
 */

$fieldName = $this->getFieldName();
$variants = $this->getAnswerVariants();
?>

<?=$this->getQuestionText()?>
<?php foreach($variants as $variant):?>
    <div class="radio <?=$this->getAdditionalClass()?>">
        <label>
            <input
                type="radio"
                name="<?=$fieldName?>"
                value="<?=$variant->getValue()?>"
                <?=($variant->isChecked() ? "checked" : "")?>
            >
            <?=$variant->getText()?>
        </label>
    </div>
<?endforeach?>