<?php
use Common\Tools;
use Common\FieldView\Field as View;
if(!Tools::isViewAssetInRightContext(get_called_class(), View::class)) return;
/**
 * @var View $this
 */
$text = $this->getQuestionText();
?>

<?if(!empty($text)):?>
    <label for="<?=$this->getId()?>"><?=$text?></label>
<?endif?>