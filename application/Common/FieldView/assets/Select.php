<?php
use Common\Tools;
use Common\FieldView\Select as View;
if(!Tools::isViewAssetInRightContext(get_called_class(), View::class)) return;
/**
 * @var View $this
 */

$variants = $this->getAnswerVariants();
$id = $this->getId();
?>

<?parent::render()?>
<select
    name="<?=$this->getFieldName()?>"
    id="<?=$id?>"
    class="form-control <?=$this->getAdditionalClass()?>"
>
    <?php foreach($variants as $variant):?>
        <option
            value="<?=$variant->getValue()?>"
            <?=($variant->isChecked() ? "checked" : "")?>
        ><?=$variant->getText()?></option>
    <?endforeach?>
</select>