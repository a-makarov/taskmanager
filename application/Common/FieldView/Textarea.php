<?php

namespace Common\FieldView;

class Textarea extends Field
{
    public function render()
    {
        include __DIR__ . "/assets/Textarea.php";
    }
}