<?php

namespace Common\FieldView;


class AnswerVariant
{
    /**
     * @var string
     */
    protected $value;

    /**
     * @var string
     */
    protected $text;

    /**
     * @var bool
     */
    protected $checked = false;
    
    public function __construct($value = null, $text = null)
    {
        $this
            ->setValue($value)
            ->setText($text);
    }

    /**
     * @return string
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * @param string $value
     * @return $this
     */
    public function setValue($value)
    {
        $this->value = $value;
        return $this;
    }

    /**
     * @return string
     */
    public function getText()
    {
        return $this->text;
    }

    /**
     * @param string $text
     * @return $this
     */
    public function setText($text)
    {
        $this->text = $text;
        return $this;
    }

    /**
     * @return bool
     */
    public function isChecked()
    {
        return $this->checked;
    }

    /**
     * @param $checked
     * @return $this
     */
    public function setChecked($checked)
    {
        $this->checked = $checked;
        return $this;
    }
}