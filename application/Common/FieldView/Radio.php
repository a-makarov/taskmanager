<?php

namespace Common\FieldView;


use Common\FieldView\Traits\MultiVariantField as MultiVariantFieldTrait;

class Radio extends Field
{
    use MultiVariantFieldTrait;

    function render()
    {
        include __DIR__ . "/assets/Radio.php";
    }
}