<?php

namespace Common\FieldView;


class Checkbox extends Input
{
    function getInputType()
    {
        return "checkbox";
    }
}