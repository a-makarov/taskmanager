<?php

namespace Common\FieldView\Traits;


use Common\FieldView\AnswerVariant;

trait MultiVariantField
{
    /**
     * @var AnswerVariant[]
     */
    protected $answerVariants = array();

    /**
     * @return AnswerVariant[]
     */
    public function getAnswerVariants()
    {
        return $this->answerVariants;
    }

    /**
     * @param AnswerVariant[] $variants
     * @return $this
     */
    public function setAnswerVariants($variants)
    {
        $this->answerVariants = $variants;
        $this->getCheckedAnswerVariant()->setChecked(true);
        return $this;
    }

    /**
     * @param AnswerVariant $variant
     * @return $this
     */
    public function addAnswerVariant (AnswerVariant $variant)
    {
        $this->answerVariants[] = $variant;
        return $this;
    }

    /**
     * @return AnswerVariant|null
     */
    protected function getCheckedAnswerVariant ()
    {
        $variants = $this->getAnswerVariants();
        foreach ($variants as $variant) {
            if ($variant->getValue() == $this->getValue()) {
                return $variant;
            }
        }
        return reset($this->answerVariants);
    }
}