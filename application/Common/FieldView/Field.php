<?php

namespace Common\FieldView;

use Base\View as BaseView;

abstract class Field extends BaseView
{
    /**
     * @var string
     */
    protected $fieldName;

    /**
     * @var string
     */
    protected $value;

    /**
     * @var string
     */
    protected $questionText;

    /**
     * @var string
     */
    protected $additionalClass;

    /**
     * @return string
     */
    public function getAdditionalClass()
    {
        return $this->additionalClass;
    }

    /**
     * @param string $additionalClass
     * @return $this
     */
    public function setAdditionalClass($additionalClass)
    {
        $this->additionalClass = $additionalClass;
        return $this;
    }

    /**
     * @param string $id
     * @return $this
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string
     */
    public function getFieldName()
    {
        return $this->fieldName;
    }

    /**
     * @param string $fieldName
     * @return $this
     */
    public function setFieldName($fieldName)
    {
        $this->fieldName = $fieldName;
        return $this;
    }

    /**
     * @return string
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * @param string $value
     * @return $this
     */
    public function setValue($value)
    {
        $this->value = $value;
        return $this;
    }

    /**
     * @return string
     */
    public function getQuestionText()
    {
        return $this->questionText;
    }

    /**
     * @param string $questionText
     * @return $this
     */
    public function setQuestionText($questionText)
    {
        $this->questionText = $questionText;
        return $this;
    }
    
    public function render()
    {
        include __DIR__ . "/assets/Field.php";
    }
}