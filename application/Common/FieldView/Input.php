<?php

namespace Common\FieldView;


abstract class Input extends Field
{
    abstract public function getInputType();
    
    public function render()
    {
        include __DIR__ . "/assets/Input.php";
    }
}