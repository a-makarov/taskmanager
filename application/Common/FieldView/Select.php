<?php

namespace Common\FieldView;

use Common\FieldView\Traits\MultiVariantField as MultiVariantFieldTrait;

class Select extends Field
{
    use MultiVariantFieldTrait;
    
    function render()
    {
        include __DIR__ . "/assets/Select.php";
    }
}