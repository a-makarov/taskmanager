<?php

namespace Common;


class Alphabet
{
    const LATIN = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";

    const NUMBER = "0123456789";

    const SYMBOL = "?!@#$%^&*()-_=+[]{}<>:;\"'.,\\/|";

    /**
     * @return string
     */
    public static function getTokenAlphabet ()
    {
        $alphabet = "";
        $alphabet .= self::LATIN;
        $alphabet .= self::NUMBER;
        $alphabet .= self::SYMBOL;
        return $alphabet;
    }

    /**
     * @param int $length
     * @param string $alphabet
     * @return string
     */
    public static function generateRandomString ($length, $alphabet = null)
    {
        if (empty($alphabet)) {
            $alphabet = self::getTokenAlphabet();
        }
        $string = "";
        while (mb_strlen($string) < $length) {
            $charNumber = mt_rand(0, mb_strlen($alphabet));
            $char = mb_substr($alphabet, $charNumber, 1);
            $string .= $char;
        }
        return $string;
    }
}
