<?php

namespace Common\Message\Controller;


class Success extends Controller
{
    protected $additionalCSSClass = "message-success";

    protected $title = "Успешно";
}