<?php

namespace Common\Message\Controller;


class Error extends Controller
{
    protected $additionalCSSClass = "message-error";

    protected $title = "Ошибка";
}