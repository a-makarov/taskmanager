<?php

namespace Common\Message\Controller;


use Common\Tools;
use Common\Message\View\View;
use Base\Controller as BaseController;

/**
 * @method View getView()
 */
class Controller extends BaseController
{
    public function declareViewInstance()
    {
        return new View();
    }

    /**
     * @var null|string
     */
    protected $additionalCSSClass;

    /**
     * @var null|string
     */
    protected $title;

    /**
     * @var array
     */
    protected $texts = array();

    /**
     * Controller constructor.
     * @param string $texts
     * @param string $title
     */
    public function __construct($texts = null, $title = null)
    {
        parent::__construct();
        if (!is_null($title)) {
            $this->setTitle($title);
        }
        if (!is_null($texts)) {
            $texts = Tools::wrapArrayIfNotItIs($texts);
            $this->setTexts($texts);
        }
    }

    /**
     * @return array
     */
    public function getTexts()
    {
        return $this->texts;
    }

    /**
     * @param string[]|string $texts
     */
    public function setTexts($texts)
    {
        $this->texts = Tools::wrapArrayIfNotItIs($texts);
    }

    /**
     * @param string $text
     */
    public function addText($text)
    {
        $this->texts[] = $text;
    }

    /**
     * @alias $this->setTexts()
     * @param string[]|string $text
     */
    public function setText($text)
    {
        $this->setTexts($text);
    }

    /**
     * @return null|string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param string $title
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * @return null|string
     */
    public function getAdditionalCSSClass()
    {
        return $this->additionalCSSClass;
    }

    /**
     * @param string $additionalCSSClass
     */
    public function setAdditionalCSSClass($additionalCSSClass)
    {
        $this->additionalCSSClass = $additionalCSSClass;
    }

    /**
     * render it's state
     */
    public function init ()
    {
        $this->getView()
            ->setTitle($this->title)
            ->setTexts($this->texts)
            ->setAdditionalCSSClass($this->additionalCSSClass);
        parent::init();
    }

    /**
     * @return string
     */
    public function __toString()
    {
        ob_start();
        $this->init();
        return ob_get_clean();
    }
}