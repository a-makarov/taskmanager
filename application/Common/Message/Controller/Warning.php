<?php

namespace Common\Message\Controller;


class Warning extends Controller
{
    protected $additionalCSSClass = "message-warning";

    protected $title = "Предупреждение";
}