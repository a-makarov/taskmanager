<?php

namespace Common\Message\View;


use Base\View as BaseView;

class View extends BaseView
{
    /**
     * @var string[]
     */
    protected $texts;

    /**
     * @var string
     */
    protected $title;

    /**
     * @var string
     */
    protected $additionalCSSClass;

    /**
     * @return string[]
     */
    public function getTexts()
    {
        return $this->texts;
    }

    /**
     * @param \string[] $texts
     * @return $this
     */
    public function setTexts($texts)
    {
        $this->texts = $texts;
        return $this;
    }

    /**
     * @return string
     */
    public function getAdditionalCSSClass()
    {
        return $this->additionalCSSClass;
    }

    /**
     * @param string $additionalCSSClass
     * @return $this
     */
    public function setAdditionalCSSClass($additionalCSSClass)
    {
        $this->additionalCSSClass = $additionalCSSClass;
        return $this;
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param string $title
     * @return $this
     */
    public function setTitle($title)
    {
        $this->title = $title;
        return $this;
    }

    public function render ()
    {
        include __DIR__ . "/assets/View.php";
    }
}