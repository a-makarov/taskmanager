<?php
use Common\Tools;
use Common\Message\View\View;
if(!Tools::isViewAssetInRightContext(get_called_class(), View::class)) return;
/**
 * @var View $this
 */
?>
<div class="message <?=$this->additionalCSSClass?>">
    <?php if(!empty($this->title)):?>
        <div class="title"><?=$this->title?></div>
    <?php endif?>
    <?=implode("<br/>", $this->texts)?>
</div>
