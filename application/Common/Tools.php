<?php

namespace Common;

use Respect\Validation\Validator;

class Tools
{
    /**
     * @param &$var mixed
     * @return array($var)
     */
    public static function wrapArrayIfNotItIs(&$var) {
        if (!is_array($var)) {
            $var = array($var);
        }
        return $var;
    }

    /**
     * @param string $url
     */
    public static function redirect ($url)
    {
        global $layout;
        $layout->cleanBuffer();
        header("location: $url");
        die();
    }

    /**
     * redirect to current url
     */
    public static function refresh ()
    {
        $path = self::getCurrentPath();
        self::redirect($path);
    }

    /**
     * @return string
     */
    public static function getCurrentPath ()
    {
        return $_SERVER["REQUEST_URI"];
    }

    public static function isPostRequest ()
    {
        return $_SERVER["REQUEST_METHOD"] == "POST";
    }

    public static function isViewAssetInRightContext ($childClass, $parentClass) {
        return $childClass == $parentClass || is_subclass_of($childClass, $parentClass);
    }
}
