<?php

namespace Auth\View;


use Base\View as BaseView;
use Auth\Controller\Form as FormController;
use Auth\Controller\Login as LoginController;
use Auth\Controller\Register as RegisterController;
use Common\FieldView\AnswerVariant as FieldAnswerVariant;
use Common\FieldView\Select as SelectField;

class GettingStarted extends BaseView
{
    /**
     * @var FormController[]
     */
    protected $formControllers;

    /**
     * @var FormController
     */
    protected $activeFormController;

    /**
     * @var SelectField
     */
    protected $formSwitcherSelect;

    public function __construct()
    {
        parent::__construct();
        $this->formControllers = array(
            new LoginController(),
            new RegisterController()
        );
        $this->determineActiveFormController();
        $this->initFormSwitcherSelect();
    }

    /**
     * @return SelectField
     */
    public function getFormSwitcherSelect()
    {
        return $this->formSwitcherSelect;
    }

    /**
     * @return FormController[]
     */
    public function getFormControllers()
    {
        return $this->formControllers;
    }

    public function determineActiveFormController()
    {
        $this->activeFormController = $this->getProcessingFormController();
        if (is_null($this->activeFormController)) {
            $this->activeFormController = reset($this->formControllers);
        }
        $this->activeFormController->setViewActive(true);
    }


    public function initFormSwitcherSelect()
    {
        $variants = array();
        
        foreach ($this->formControllers as $formController) {
            $view = $formController->getView();
            $variants[] = new FieldAnswerVariant($view->getId(), $view->getSwitcherText());
        }
        $presetValue = $this->activeFormController->getView()->getId();
        $this->formSwitcherSelect = (new SelectField)
            ->setAdditionalClass("getting-started-form-switcher pull-right")
            ->setAnswerVariants($variants)
            ->setValue($presetValue);
    }

    /**
     * @return FormController|null
     */
    public function getProcessingFormController() {
        foreach ($this->formControllers as $formController) {
            if ($formController->needPostProcessing()) {
                reset($this->formControllers);
                return $formController;
            }
        }
        return null;
    }
    
    public function render()
    {
        include __DIR__ . "/assets/GettingStarted.php";
    }
}