<?php

namespace Auth\View;


use Base\Form\View\View as BaseFormView;

abstract class Form extends BaseFormView
{
    /**
     * @var bool
     */
    protected $active = false;

    /**
     * @return bool
     */
    public function getActive ()
    {
        return $this->active;
    }

    /**
     * @param bool $active
     * @return $this
     */
    public function setActive ($active)
    {
        $this->active = $active;
        return $this;
    }
    
    abstract public function getTitle();
    abstract public function getSwitcherText();
    
    public function render()
    {
        include __DIR__ . "/assets/Form.php";
    }
}