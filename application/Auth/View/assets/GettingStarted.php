<?php
use Common\Tools;
use Auth\View\GettingStarted as View;
if(!Tools::isViewAssetInRightContext(get_called_class(), View::class)) return;
/**
 * @var View $this
 */
global $layout;
$layout->setAdditionalClassForMain("diagonal-strips-bg");
?>
<div class="getting-started">
    <?foreach ($this->getFormControllers() as $formController):?>
        <?$formController->init()?>
    <?endforeach?>
    <div class="getting-started-form-list">
        <?foreach ($this->getFormControllers() as $formController):
            $view = $formController->getView()?>
            <a
                href="#"
                class="getting-started-form-switcher <?=$view->getActive() ? "active" : ""?>"
                data-form="<?=$view->getId()?>"
            ><?=$view->getSwitcherText()?></a>
        <?endforeach?>
    </div>
</div>
