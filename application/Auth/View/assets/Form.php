<?php
use Common\Tools;
use Auth\View\Form as View;
if(!Tools::isViewAssetInRightContext(get_called_class(), View::class)) return;
/**
 * @var View $this
 */
?>
<div class="getting-started-form <?=($this->getActive() ? "active" : "")?>" id="<?=$this->getId()?>">
    <h2><?=$this->getTitle()?></h2>
    <?parent::render()?>
</div>