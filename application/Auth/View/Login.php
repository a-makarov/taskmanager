<?php

namespace Auth\View;

use Localization\Texts;


class Login extends Form
{
    protected $active = true;
    
    public $buttonText = Texts::TO_LOGIN;
    
    public function getTitle()
    {
        return Texts::ENTRY;
    }

    public function getSwitcherText()
    {
        return Texts::TO_LOGIN;
    }
}