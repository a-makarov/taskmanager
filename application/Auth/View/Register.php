<?php

namespace Auth\View;

use Localization\Texts;


class Register extends Form
{
    public $buttonText = Texts::TO_REGISTER;
    
    public function getTitle()
    {
        return Texts::REGISTRATION;
    }

    public function getSwitcherText()
    {
        return Texts::TO_REGISTER;
    }
}