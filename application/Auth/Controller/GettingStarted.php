<?php

namespace Auth\Controller;

use Auth\View\GettingStarted as GettingStartedView;
use Base\Controller as BaseController;

class GettingStarted extends BaseController
{
    protected function declareViewInstance ()
    {
       return new GettingStartedView();
    }
}