<?php

namespace Auth\Controller;


use Auth\Model\Model;
use Auth\View\Register as RegisterView;
use Auth\Model\FormFieldMap\Register as RegisterFormFieldMap;
use Common\Message\Controller\Error as ErrorMessage;
use Common\Tools;
use Respect\Validation\Exceptions\ValidationException;

/**
 * @method RegisterFormFieldMap getFormFieldMap()
 */
class Register extends Form
{
    protected function declareViewInstance ()
    {
        return new RegisterView();
    }
    
    public function declareFormFieldMap()
    {
        return new RegisterFormFieldMap();
    }

    public function processPost()
    {
        if (!$this->needPostProcessing()) {
            return;
        }
        try {
            $fieldMap = $this->getFormFieldMap();
            $email = $fieldMap->email->getValue();
            $password = $fieldMap->password->getValue();
            $confirmPassword = $fieldMap->confirmPassword->getValue();
            
            Model::register($email, $password, $confirmPassword);
            Tools::redirect("/profile");
        } catch (ValidationException $e) {
            $errorMessage = new ErrorMessage($e->getMainMessage());
            $this->getView()->addMessage($errorMessage);
        }
    }
}