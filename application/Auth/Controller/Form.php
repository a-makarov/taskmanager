<?php

namespace Auth\Controller;


use Base\Form\Controller\Controller as BaseFormController;
use Auth\View\Form as FormView;

/**
 * @method FormView getView()
 */
abstract class Form extends BaseFormController
{
    /**
     * @var bool
     */
    protected $viewActive = false;

    public function init()
    {
        $this->processPost();
        $this->getView()->setActive($this->getViewActive());
        parent::init();
    }
    
    /**
     * @return bool
     */
    public function getViewActive()
    {
        return $this->viewActive;
    }
    
    /**
     * @param bool $viewActive
     * @return $this
     */
    public function setViewActive($viewActive)
    {
        $this->viewActive = $viewActive;
        return $this;
    }
}