<?php

namespace Auth\Controller;


use Auth\Model\FormFieldMap\Login as FormFieldMap;
use Auth\Model\Model;
use Auth\View\Login as View;
use Common\Message\Controller\Error as ErrorMessage;
use Common\Tools;
use Respect\Validation\Exceptions\ValidationException;

/**
 * @method FormFieldMap getFormFieldMap()
 */
class Login extends Form
{
    protected function declareViewInstance ()
    {
        return new View();
    }
    
    public function declareFormFieldMap()
    {
        return new FormFieldMap();
    }

    public function processPost()
    {
        if (!$this->needPostProcessing()) {
            return;
        }
        try {
            $fieldMap = $this->getFormFieldMap();
            $email = $fieldMap->email->getValue();
            $password = $fieldMap->password->getValue();

            Model::login($email, $password);
            Tools::refresh();
        } catch (ValidationException $e) {
            $errorMessage = new ErrorMessage($e->getMainMessage());
            $this->getView()->addMessage($errorMessage);
        }
    }
}