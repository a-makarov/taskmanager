<?php

namespace Auth\Model\FormFieldMap;


use Base\Form\Model\FormFieldMap as BaseFormFieldMap;
use Common\FieldView\Email;
use Common\FieldView\Password;
use Localization\Texts;

class Login extends BaseFormFieldMap
{
    /**
     * @var Email
     */
    public $email;

    /**
     * @var Password
     */
    public $password;
    
    public function __construct()
    {
        $this->email = new Email();
        $this->email
            ->setFieldName("email")
            ->setQuestionText(Texts::EMAIL);
        
        $this->password = new Password();
        $this->password
            ->setFieldName("password")
            ->setQuestionText(Texts::PASSWORD);

        $this->fillFromPostRequest();
    }
}