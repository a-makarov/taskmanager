<?php

namespace Auth\Model;

use Doctrine\ORM\QueryBuilder;
use Common\Alphabet;


class Cryptography
{
    const SALT_LENGTH = 32;

    const TOKEN_LENGTH = 32;

    const SHA_HASH_LENGTH = 512;

    /**
     * Method for hashing data by default algorithm
     * @param mixed $input
     * @return string
     */
    public static function hash ($input)
    {
        $hashAlgorithm = self::getHashAlgorithm();
        return hash($hashAlgorithm, $input);
    }

    /**
     * @param string $password
     * @param string $salt
     * @return string
     */
    public static function hashToken ($password, $salt)
    {
        $passwordHash = self::hash($password);
        return self::hash($salt . $passwordHash);
    }

    /**
     * @return string
     */
    public static function getHashAlgorithm ()
    {
        return "sha" . self::SHA_HASH_LENGTH;
    }

    /**
     * @return string
     */
    public static function generateSalt ()
    {
        return Alphabet::generateRandomString(self::SALT_LENGTH);
    }

    /**
     * @return string
     */
    public static function generateToken ()
    {
        return Alphabet::generateRandomString(self::TOKEN_LENGTH);
    }

    /**
     * @param string &$token
     * @param &$salt
     * @param string &$hash
     * @return void
     */
    public static function generateSession (&$token, &$salt, &$hash)
    {
        $token = self::generateToken();
        $salt = self::generateSalt();
        $hash = self::hashToken($token, $salt);
    }

    /**
     * @param string $saltField
     * @param string $hashField
     * @return string
     */
    public static function createAuthenticateFilterString ($saltField, $hashField)
    {
        $shaLength = self::SHA_HASH_LENGTH;
        return "SHA2(CONCAT($saltField, :inputHash), $shaLength) = $hashField";
    }

    /**
     * @param QueryBuilder $query
     * @param string $saltField
     * @param string $hashField
     * @param $input
     * @return QueryBuilder
     */
    public static function setAuthenticateFilter (QueryBuilder $query, $hashField, $saltField, $input)
    {
        $inputHash = self::hash($input);
        $filter = self::createAuthenticateFilterString($saltField, $hashField);
        return $query
            ->andWhere($filter)
            ->setParameter("inputHash", $inputHash);
    }
}
