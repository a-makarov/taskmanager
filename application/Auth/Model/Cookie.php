<?php

namespace Auth\Model;

class Cookie
{
    const COOKIE_KEY__USER_ID = "user_id";

    const COOKIE_KEY__TOKEN = "auth_token";

    const AUTH_EXPIRE_TIME = 86400 * 30;

    /**
     * @return int|null
     */
    public static function getId ()
    {
        if (isset($_COOKIE[self::COOKIE_KEY__USER_ID])) {
            $id = intval($_COOKIE[self::COOKIE_KEY__USER_ID]);
            if ($id > 0) {
                return $id;
            }
        }
        return  null;
    }

    /**
     * @param int $id
     * @return void
     */
    public static function setId ($id)
    {
        setcookie(self::COOKIE_KEY__USER_ID, $id, time() + self::AUTH_EXPIRE_TIME);
    }

    /**
     * @return string|null
     */
    public static function getToken ()
    {
        return empty($_COOKIE[self::COOKIE_KEY__TOKEN]) ? null : $_COOKIE[self::COOKIE_KEY__TOKEN];
    }

    /**
     * @param string $token
     * @return void
     */
    public static function setToken ($token)
    {
        setcookie(self::COOKIE_KEY__TOKEN, $token, time() + self::AUTH_EXPIRE_TIME);
    }

    /**
     * @param int $id
     * @param string $token
     * @return void
     */
    public static function setAuthData ($id, $token)
    {
        self::setId($id);
        self::setToken($token);
    }

    /**
     * @return void
     */
    public static function unsetAuthData ()
    {
        setcookie(self::COOKIE_KEY__USER_ID, "", 0);
        setcookie(self::COOKIE_KEY__TOKEN, "", 0);
    }

    /**
     * The method verifies that the user id and the authorize token exists in the cookie
     * @return bool
     */
    public static function clientHasAuthData ()
    {
        return isset($_COOKIE[self::COOKIE_KEY__USER_ID]) && isset($_COOKIE[self::COOKIE_KEY__TOKEN]);
    }

    /**
     * @return void
     */
    public static function prolong ()
    {
        $id = self::getId();
        $token = self::getToken();
        self::setAuthData($id, $token);
    }
}
