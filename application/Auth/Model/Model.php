<?php

namespace Auth\Model;

use Localization\Texts;
use Respect\Validation\Validator;
use Session\Model\EntityDataMap as SessionEntity;
use Session\Model\Model as SessionModel;
use User\Model\EntityDataMap as UserEntity;
use User\Model\Model as UserModel;

class Model
{
    const MIN_PASSWORD_LENGTH = 6;

    /**
     * @param string $email
     * @param string $password
     * @param string $confirmPassword
     * @return UserEntity
     */
    public static function register ($email, $password, $confirmPassword)
    {
        self::assertEqualPasswords($password, $confirmPassword);
        self::assertPasswordFormatted($password);
        $user = UserModel::add($email, $password);
        self::authorize($user);
        return $user;
    }

    /**
     * @param string $email
     * @param string $password
     * @return SessionEntity
     */
    public static function login ($email, $password)
    {
        $user = UserModel::getByAuth($email, $password);
        self::assertUserByAuthExist($user);
        return self::authorize($user);
    }

    /**
     * @return void
     */
    public static function logout ()
    {
        SessionModel::closeCurrent();
    }

    /**
     * The method verifies that the current user has session with the token
     * @return bool
     */
    public static function authenticate ()
    {
        $session = SessionModel::getCurrent();
        return !is_null($session);
    }

    /**
     * @param UserEntity $user
     * @return SessionEntity
     */
    public static function authorize (UserEntity $user)
    {
        SessionModel::closeCurrent();
        $session = new SessionEntity();
        $session
            ->user($user)
            ->createAuthData()
            ->save();
        Cookie::setAuthData($user->id(), $session->token());
        return $session;
    }

    /**
     * @param $password
     * @param $confirmPassword
     */
    protected static function assertEqualPasswords ($password, $confirmPassword)
    {
        (new Validator)
            ->equals($password)
            ->setTemplate(Texts::PASSWORDS_NOT_EQUAL)
            ->assert($confirmPassword);
    }

    /**
     * @param $user
     */
    protected static function assertUserByAuthExist ($user)
    {
        (new Validator)
            ->not(Validator::nullType())
            ->setTemplate(Texts::INVALID_LOGIN_OR_PASSWORD)
            ->assert($user);
    }

    /**
     * @param $password
     */
    protected static function assertPasswordFormatted ($password)
    {
        (new Validator())
            ->length(self::MIN_PASSWORD_LENGTH)
            ->setTemplate(Texts::PASSWORD_MUST_BE_FORMATTED)
            ->assert($password);
    }
}
