<?php

namespace Session\Model;

use Auth\Model\Cryptography;
use Common\DataMap\Entity;
use DateTime;
use User\Model\EntityDataMap as UserEntity;

/**
 * @Entity
 * @Table(name="sessions")
 * @HasLifeCycleCallbacks
 *
 * @method int id()
 * @method string    |$this tokenHash(string $value = null)
 * @method string    |$this tokenSalt(string $value = null)
 * @method string    |$this userAgent(string $value = null)
 * @method UserEntity|$this user(UserEntity $value = null)
 * @method DateTime  |$this lastActivityDate(DateTime $value = null)
 * @method bool      |$this able(bool $value = null)
 **/
class EntityDataMap extends Entity
{
    /**
     * @var int
     * @Id
     * @Column(type="integer")
     * @GeneratedValue
     */
    protected $id;

    /**
     * @var int
     * @ManyToOne(targetEntity="User\Model\EntityDataMap", inversedBy="sessions")
     */
    protected $user;

    /**
     * @var string
     * @Column(
     *     type="string",
     *     length=128
     * )
     */
    protected $token_hash;

    /**
     * @var string
     * @Column(
     *     type="string",
     *     length=32
     * )
     */
    protected $token_salt;

    /**
     * @var string
     * @Column(
     *     type="string",
     *     nullable=true
     * )
     */
    protected $user_agent;

    /**
     * @var DateTime
     * @Column(type="datetime")
     */
    protected $last_activity_date;

    /**
     * @var bool
     * @Column(type="boolean")
     */
    protected $able = true;

    /**
     * Not a data map field! It fills only when the object assembling
     * and only when auth data creating.
     *
     * @var string
     */
    protected $token;

    /**
     * @PrePersist
     */
    function onPrePersist()
    {
        $this->lastActivityDate(new DateTime());
    }

    /**
     * creates and sets token, salt and hash
     * @return $this
     */
    public function createAuthData()
    {
        Cryptography::generateSession($token, $salt, $hash);
        $this
            ->tokenSalt($salt)
            ->tokenHash($hash)
            ->userAgent($_SERVER["HTTP_USER_AGENT"]);
        $this->token = $token;
        return $this;
    }

    /**
     * Method sets able as false
     * @return $this
     */
    public function close ()
    {
        $this->able = false;
        return $this;
    }

    /**
     * @return string
     */
    public function token ()
    {
        return $this->token;
    }

    /**
     * @return $this
     */
    public function prolong ()
    {
        return $this->lastActivityDate(new DateTime());
    }
}
