<?php

namespace Session\Model;

use Auth\Model\Cookie;
use Auth\Model\Cryptography;
use DateTime;
use Respect\Validation\Validator;
use Session\Model\EntityDataMap as SessionEntity;

class Model
{
    /**
     * @return SessionEntity|null
     */
    public static function getCurrent()
    {
        if (!Cookie::clientHasAuthData()) {
            return null;
        }
        $userId = Cookie::getId();
        $userToken = Cookie::getToken();
        global $EM;
        $queryBuilder = $EM->createQueryBuilder()
            ->select("s")
            ->from(SessionEntity::class, "s")
            ->where("s.user = :user")
            ->setParameter("user", $userId)
            ->setMaxResults(1);
        Cryptography::setAuthenticateFilter($queryBuilder, "s.token_hash", "s.token_salt", $userToken);
        $result = $queryBuilder->getQuery()->getResult();
        return array_shift($result);
    }

    /**
     * @return void
     */
    public static function closeCurrent ()
    {
        $session = self::getCurrent();
        if (is_null($session)) {
            return;
        }
        $session->close()->save();
        Cookie::unsetAuthData();
    }

    /**
     * @return void
     */
    public static function prolongCurrent ()
    {
        $session = self::getCurrent();
        self::assertSessionExist($session);
        $session
            ->lastActivityDate(new DateTime)
            ->save();
        Cookie::prolong();
    }

    /**
     * @param $session
     */
    private static function assertSessionExist ($session)
    {
        $validator = Validator::not(Validator::nullType());
        $validator->setTemplate("Session not exist");
        $validator->assert($session);
    }
}
