<?php

use Doctrine\ORM\Tools\Setup;
use Doctrine\ORM\EntityManager;

$dbParams = array(
    "driver"   => "pdo_mysql",
    "dbname"   => "taskmanager",
    "user"     => "root",
    "password" => "root"
);

$config = Setup::createAnnotationMetadataConfiguration(array(__DIR__), true);
$config->addCustomStringFunction("SHA2", "DoctrineExtensions\\Query\\Mysql\\Sha2");

global $EM;
$EM = EntityManager::create($dbParams, $config);
