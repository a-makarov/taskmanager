<?php

namespace Tests\Main;

use Common\Tools;

class ToolsTest extends \PHPUnit_Framework_TestCase
{
    public function wrapArrayIfNotItIsTest ()
    {
        $var = "foo";
        Tools::wrapArrayIfNotItIs($var);
        $this->assertEquals(array("foo"), $var);

        $var = array("foo");
        Tools::wrapArrayIfNotItIs($var);
        $this->assertEquals(array("foo"), $var);
    }
}
