<?php

namespace Tests\User\Model;

use \User\Model\Cryptography;

class CryptographyTest extends \PHPUnit_Framework_TestCase
{
    public function generateRandomStringTest ()
    {
        $length = 32;
        $alphabet = "QWERTY";
        $string = Cryptography::generateRandomString($length, $alphabet);
        $this->assertEquals($length, strlen($string), "It is not the same string length");
        $this->assertRegExp("/[$alphabet]+/", $string, "It is not correspond to the alphabet");
    }

    /**
     * @depends generateRandomStringTest
     */
    public function generateSaltTest ()
    {
        $salt = Cryptography::generateSalt();
        $length = Cryptography::SALT_LENGTH;
        $this->assertEquals($length, strlen($salt), "It is not the same string length");
    }

    // TODO testHashPassword
}
