<?php

namespace Tests\DataMap;

use Common\DataMap\Entity;

class EntityTest extends \PHPUnit_Framework_TestCase
{
    public function determinePropertyNameTest ()
    {
        $testInstance = new TestClass();
        $this->assertEquals($testInstance->determinePropertyName("lower_under"), "lower_under");
        $this->assertEquals($testInstance->determinePropertyName("lowerUnder"), "lower_under");
        $this->assertEquals($testInstance->determinePropertyName("camelCase"), "camelCase");
    }

    /**
     * @depends determinePropertyNameTest
     */
    public function magicianMutatorTest ()
    {
        $testInstance = new TestClass();
        $this->assertEquals($testInstance->lower_under, $testInstance->lowerUnder());
        $testInstance->lowerUnder(false);
        $this->assertEquals($testInstance->lower_under, false);
    }
}

/**
 * Test class for checking magician methods
 */
class TestClass extends Entity
{
    public $lower_under = true;
    public $camelCase = false;
}